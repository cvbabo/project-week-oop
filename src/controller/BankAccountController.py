from src.model.Bank import Bank
from src.model.Account import Account


class BankAccountController:

    bankDict = {}
    i = 1
    accountDict = {}
    a = 1

    def __init__(self):
        self = self

    def getBanks(self):
        return self.bankDict

    def createNewBank(self, bankName, blz):
        bankIndex = "bank"+str(self.i)
        bnk = Bank(bankName, blz)
        self.bankDict[bankIndex] = bnk
        self.i += 1
        print(bankName + " angelegt mit Key: " + bankIndex)

    def deleteBank(self, key):
        if(key in self.bankDict.keys()):
            del self.bankDict[key]
            print(key + " was successfully deleted.")
        else:
            print("Key does not exist.")

    def checkExistingBank(self, key):
        if (key in self.bankDict.keys()):
            return True
        else:
            return False

    def getAccounts(self):
        return self.accountDict

    def createNewAccount(self, fName, lName, bnk, type, amt):
        accountIndex = "account"+str(self.a)
        acc = Account(fName, lName, bnk, type, amt)
        self.accountDict[accountIndex] = acc
        self.a += 1
        print("Konto für " + lName + ", " + fName + " unter " + accountIndex + " angelegt.")

    def deleteAccount(self, key):
        if(key in self.accountDict.keys()):
            del self.accountDict[key]
            print(key + " was successfully deleted.")
        else:
            print("Key does not exist.")

    def checkExistingAccount(self, key):
        if (key in self.accountDict.keys()):
            return True
        else:
            return False