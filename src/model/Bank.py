class Bank:

    def __init__(self, institut, bic):
        self.inst = institut
        self.bic = bic

    def getInstituteName(self):
        return self.inst

    def getBIC(self):
        return self.bic