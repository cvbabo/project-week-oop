from src.model.Bank import Bank

class Account:

    def __init__(self, firstName, lastName, Bank, type, amount):
        self.fName = firstName
        self.lName = lastName
        self.bnk = Bank
        self.type = type
        self.amt = amount

    def getBank(self):
        return self.bnk

    def getName(self):
        return (self.fName + " " + self.lName)

    def getType(self):
        return self.type

    def getAmount(self):
        return self.amt

    def deposit(self, value):
        self.amt += value

    def withdraw(self, value):
        if(self.amt < value):
            print("Not enough money. Maximum amount to withdraw: " + str(self.amt))
        else:
            self.amt -= value

    def transfer(self, value, account):
        if (self.amt < value):
            print("Not enough money. Maximum amount to transfer: " + str(self.amt))
        else:
            account.deposit(value)
            self.withdraw(value)