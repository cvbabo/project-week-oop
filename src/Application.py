from src.model.Bank import Bank
from src.model.Account import Account
from src.controller.BankAccountController import BankAccountController
import time

exitApp = False
appController = BankAccountController()

while exitApp == False:
    exitBnks = False
    exitAccounts = False
    print("")
    print("What would like to do?")
    print("1 - Manage Banks")
    print("2 - Manage Accounts")
    print("9 - Exit")
    print("")

    userInput = input()

    if userInput == "1":
        while exitBnks == False:
            print("")
            print("What would like to do?")
            print("1 - Create New Bank")
            print("2 - Delete Existing Bank")
            print("3 - See All Banks")
            print("9 - Return To Home")
            print("")

            userInputBnk = input()

            if userInputBnk == "1":
                institute = input("What is the bank's name?")
                bic = input("What is the bank's BIC?")
                appController.createNewBank(institute, bic)
            elif userInputBnk == "2":
                keyToDelete = input("Type in the bank's key to delete:")
                appController.deleteBank(keyToDelete)
            elif userInputBnk == "3":
                banks = appController.getBanks()
                if len(banks) == 0:
                  print("No banks available yet.")
                else:
                    for i in banks:
                        print(i + " -- Name: " + str(banks.get(i).getInstituteName()) + ", BIC: " + str(banks.get(i).getBIC()))
            elif userInputBnk == "9":
                exitBnks = True
            else:
                print("No valid operation")
                time.sleep(2)

    elif userInput == "2":
        while exitAccounts == False:
            print("")
            print("What would like to do?")
            print("1 - Create New Account")
            print("2 - Delete Existing Account")
            print("3 - See All Accounts")
            print("4 - Deposit Money to Account")
            print("5 - Withdraw Money from Account")
            print("6 - Transfer Money to other Account")
            print("9 - Return To Home")
            print("")

            userInputAcc = input()

            if userInputAcc == "1":
                banks = appController.getBanks()
                if len(banks) == 0:
                    print("No banks available yet.")
                else:
                    bank = input("What is the bank's key?")
                    if appController.checkExistingBank(bank):
                        bnk = appController.getBanks().get(bank)
                        fName = input("What is your first name?")
                        lName = input("What is your last name?")
                        accType = input("What is the account type?")
                        initialAmount = input("What is the initial amount?")
                        appController.createNewAccount(fName, lName, bnk, accType, float(initialAmount))
                    else:
                        print("Bank key does not exist.")
            elif userInputAcc == "2":
                keyToDelete = input("Type in the account's key to delete:")
                appController.deleteAccount(keyToDelete)
            elif userInputAcc == "3":
                accounts = appController.getAccounts()
                if len(accounts) == 0:
                  print("No accounts available yet.")
                else:
                    for i in accounts:
                        print(i + " -- Name: " + str(accounts.get(i).getName()) + ", Type: " + str(accounts.get(i).getType()) + ", current Amount: " + str(accounts.get(i).getAmount()) + " €")
                        print("Bank: " + str(accounts.get(i).getBank().getInstituteName()) + ", BLZ: " + str(accounts.get(i).getBank().getBIC()))
                for i in accounts:
                    print()
            elif userInputAcc == "4":
                key = input("Type in the account's key to deposit money to:")
                if appController.checkExistingAccount(key):
                    amount = input("Type in the amount you want to deposit:")
                    accounts.get(key).deposit(float(amount))
                    print(str(amount) + " € successfully deposited.")
                else:
                    print("Account not available.")
            elif userInputAcc == "5":
                key = input("Type in the account's key to withdraw money from:")
                if appController.checkExistingAccount(key):
                    amount = input("Type in the amount you want to withdraw:")
                    accounts.get(key).withdraw(float(amount))
                    print(str(amount) + " € successfully withdrawed.")
                else:
                    print("Account not available.")
            elif userInputAcc == "6":
                keyReceiver = input("Type in the receiving account's key:")
                keySender = input("Type in the sending account's key:")
                if appController.checkExistingAccount(keySender):
                    if appController.checkExistingAccount(keyReceiver):
                        amount = input("Type in the amount you want to transfer:")
                        accounts.get(keySender).transfer(float(amount), accounts.get(keyReceiver))
                        print(str(amount) + " € successfully transfered.")
                    else:
                        print("Receiving account not available.")
                else:
                    print("Sending account not available.")
            elif userInputAcc == "9":
                exitAccounts = True
            else:
                print("No valid operation")
                time.sleep(2)

    elif(userInput == "9"):
        exitApp = True

    else:
        print("No valid operation")
        time.sleep(2)